///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/sizer.h>
#include <wx/gdicmn.h>
#include <wx/string.h>
#include <wx/frame.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/ribbon/toolbar.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/ribbon/panel.h>
#include <wx/ribbon/page.h>
#include <wx/ribbon/control.h>
#include <wx/ribbon/art.h>
#include <wx/ribbon/bar.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class frmBaseline
///////////////////////////////////////////////////////////////////////////////
class frmBaseline : public wxFrame
{
	private:

	protected:

	public:
		wxBoxSizer* box;

		frmBaseline( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 1280,720 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~frmBaseline();

};

///////////////////////////////////////////////////////////////////////////////
/// Class frmSettings
///////////////////////////////////////////////////////////////////////////////
class frmSettings : public wxFrame
{
	private:

	protected:
		wxRibbonPage* m_ribbonPage2;
		wxRibbonPanel* m_ribbonPanel2;
		wxRibbonToolBar* m_ribbonToolBar1;

	public:
		wxRibbonBar* m_ribbonBar3;

		frmSettings( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 720,500 ), long style = wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxMINIMIZE|wxTAB_TRAVERSAL );

		~frmSettings();

};

