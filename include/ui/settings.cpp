//
// Created by cybik on 22-07-17.
//

#include "settings.h"

#include <iostream>

#define RIBBON_PAGE_GENERAL 0x6384+0
#define RIBBON_PAGE_ENH 0x6384+1
#define RIBBON_PAGE_WINE_VER 0x6384+2
#define RIBBON_PAGE_DXVK 0x6384+3
#define RIBBON_PAGE_SHADE 0x6384+4
#define RIBBON_PAGE_ENV 0x6384+5

#define RIBBON_TOGGLE_TEST 0x6384+6

void Settings::OnBar(wxRibbonBarEvent& evt) {
    std::cout << (evt.GetEventType() == wxEVT_RIBBONBAR_PAGE_CHANGED ? "changed" : "not") << std::endl;
    std::cout << evt.GetPage()->GetId() << std::endl;
}


Settings::Settings( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
    this->SetSizeHints( size, size );

    wxBoxSizer* bSizer2;
    bSizer2 = new wxBoxSizer( wxVERTICAL );

    m_ribbonBar3 = new wxRibbonBar( this,
                                    -1,
                                    wxDefaultPosition, size,
                                    wxRIBBON_BAR_FLOW_HORIZONTAL
                                          | wxRIBBON_BAR_SHOW_PAGE_LABELS
                                          | wxRIBBON_BAR_ALWAYS_SHOW_TABS
    );
    m_art = new wxRibbonMSWArtProvider();
    m_ribbonBar3->SetArtProvider(m_art.get());
    wxSharedPtr<wxRibbonPage> page = AddPage("General", RIBBON_PAGE_GENERAL);
    m_ribbonPanel2 = new wxRibbonPanel( page.get(), wxID_ANY, wxT("MyRibbonPanel") ,
                                        wxNullBitmap , wxDefaultPosition, size, wxRIBBON_PANEL_NO_AUTO_MINIMISE|wxRIBBON_PANEL_STRETCH );
    page = AddPage("Enhancements", RIBBON_PAGE_ENH);
    m_ribbonPanel5 = new wxRibbonPanel( page.get(), wxID_ANY, wxT("MyRibbonPanel-") ,
                                        wxNullBitmap , wxDefaultPosition, size, wxRIBBON_PANEL_NO_AUTO_MINIMISE|wxRIBBON_PANEL_STRETCH );
    page = AddPage("Runner Version", RIBBON_PAGE_WINE_VER);
    page = AddPage("DXVK", RIBBON_PAGE_DXVK);
    page = AddPage("Shaders", RIBBON_PAGE_SHADE);
    page = AddPage("Environment", RIBBON_PAGE_ENV);

    bSizer2->Add( m_ribbonBar3.get(), 0, wxEXPAND, 5 );
    m_ribbonBar3->Realize();

    Connect(wxEVT_RIBBONBAR_PAGE_CHANGED, wxRibbonBarEventHandler(Settings::OnBar), NULL, this);

    this->SetSizer( bSizer2 );
    this->Layout();

    this->Centre( wxBOTH );
}

wxSharedPtr<wxRibbonPage> Settings::AddPage(std::string title, int id) {
    wxSharedPtr<wxRibbonPage> new_page(new wxRibbonPage( m_ribbonBar3.get(),id, title.c_str(), wxNullBitmap ));
    pages.push_back(new_page);
    return new_page;
}

Settings::~Settings()
{
}
