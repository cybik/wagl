//
// Created by cybik on 22-07-17.
//

#ifndef WAGL_SETTINGS_H
#define WAGL_SETTINGS_H

// will simplify
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/sizer.h>
#include <wx/gdicmn.h>
#include <wx/string.h>
#include <wx/frame.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/ribbon/toolbar.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/ribbon/panel.h>
#include <wx/ribbon/page.h>
#include <wx/ribbon/control.h>
#include <wx/ribbon/art.h>
#include <wx/ribbon/bar.h>
#include <wx/sharedptr.h>

#include <vector>

class Settings : public wxFrame {
private:

protected:

    wxSharedPtr<wxRibbonPage> AddPage(std::string title, int id);
    std::vector<wxSharedPtr<wxRibbonPage>> pages = std::vector<wxSharedPtr<wxRibbonPage>>();

    wxSharedPtr<wxRibbonPanel> m_ribbonPanel2;
    wxRibbonPanel* m_ribbonPanel5;

    wxSharedPtr<wxRibbonArtProvider> m_art;

    void OnBar(wxRibbonBarEvent& evt);

public:
    wxSharedPtr<wxRibbonBar> m_ribbonBar3;

    Settings( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString,
              const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 720,500 ),
              long style = wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxMINIMIZE|wxTAB_TRAVERSAL
    );
    Settings(std::string title) : Settings(nullptr, wxID_ANY, title) {}

    ~Settings();

};

#endif //WAGL_SETTINGS_H
