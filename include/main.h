//
// Created by cybik on 22-07-12.
//

#ifndef WAGL_MAIN_H
#define WAGL_MAIN_H
#include <wx/app.h>
#include <wx/webview.h>

#include "noname.h"
#include <wx/ribbon/bar.h>
#include <wx/ribbon/buttonbar.h>
#include <wx/ribbon/gallery.h>
#include <wx/ribbon/toolbar.h>

#include "ui/settings.h"

class WAGL : public wxApp
{
public:
    bool OnInit();
protected:
    void OnDocumentLoaded(wxWebViewEvent& evt);
    void OnNavigationRequest(wxWebViewEvent& evt);
    void Width_Height_Workaround(wxWebViewEvent& evt);
private:
    void run_js(std::string);
    void inject_settings();
    void inject_stylesheet();
    void Do_Background_And_Size();

    void SettingsTest();

    wxWeakRef<frmBaseline> frame;
    //wxWeakRef<frmSettings> settings;
    wxWeakRef<Settings> settings;
    wxWeakRef<wxWebView> web;

    wxSharedPtr<wxRibbonBar> ribb;
    wxSharedPtr<wxRibbonPage> page1;
    wxSharedPtr<wxRibbonPanel> tricks;
};

DECLARE_APP(WAGL)
IMPLEMENT_APP(WAGL)

#endif //WAGL_MAIN_H
