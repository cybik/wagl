#include "include/main.h"

#include <iostream>
#include <wx/version.h>
#include <wx/sharedptr.h>

#include "util/Constants.h"

#include "resources/gear_image.h"
#include "resources/stylesheets.h"
#include "resources/SASSProcess.h"

#include <fmt/core.h>

#define YAAGL_SETTINGS "#yaagl-settings"

#include <httplib.h>
#include <tao/json.hpp>


void WAGL::SettingsTest() {
    if(settings == nullptr) {
        settings = (new Settings("Settings"));
    }
    settings->Show();
}

std::string generate_url() {
    return std::string("https://")
        .append(QAGL::placeholders::lowercase::first).append(".")
        .append(QAGL::placeholders::lowercase::company)
        .append(".com/launcher/10/en-us?api_url=https%3A%2F%2Fapi-os-takumi.")
        .append(QAGL::placeholders::lowercase::company)
        .append(".com%2Fhk4e_global&key=gcStgarh&prev=false");
}

void WAGL::run_js(std::string script) {
    web->RunScript(script);
}

void WAGL::Do_Background_And_Size() {
    // Use a header lib to get the URL for the background
    httplib::Client cli(QAGL::uri::https_api_host);
    auto res = cli.Get((QAGL::backgroundUri_path+QAGL::langcodes::en_US).c_str());
    const tao::json::value v = tao::json::from_string( res->body );
    auto back = v.at("data").at("adv").at("background");
    run_js("document.body.background = ('"+back.get_string()+"');");

    // run the w-h hack after background
    run_js("width = (document.getElementsByClassName('home')[0].clientWidth);" \
                 "height = (document.getElementsByClassName('home')[0].clientHeight);" \
                 "str_w_h = ''+width+' '+height; document.title = str_w_h;"
    );
}

void WAGL::inject_stylesheet() {
    run_js(fmt::format(
        "(function() {{\n" \
            "css = document.createElement('style'); css.type = 'text/css'; css.id = '{0}';\n"\
            "css.textContent = `{1}`;\n"\
            "document.head.appendChild(css);\n"\
        "}})()", "cpp-sends-their-regards", SASSProcess(idx_sass))
    );
}

// use RunScript
void WAGL::inject_settings() {
    run_js(fmt::vformat((
        "(function() {{\n" \
            "img_idle = document.createElement('img'); img_idle.src = '{0}';\n"\
            "img_idle.className = 'unactive'; img_idle.alt = 'icon';\n"\
            "img_hovr = document.createElement('img'); img_hovr.src = '{1}';\n"\
            "img_hovr.className = 'active'; img_hovr.alt = 'icon';\n"\
            "settings = document.createElement('div'); settings.id = 'settings';\n"\
            "settings.appendChild(img_idle); settings.appendChild(img_hovr);\n"\
            "document.body.appendChild(settings);\n"\
            "const t = document.getElementById(\"settings\");\n"\
            "t.onclick = () => {{ t == null || location.assign('{2}'); }},\n"\
            "t.onmouseenter = () => {{ t == null || t.classList.add(\"hovered\"); }},\n"\
            "t.onmouseleave = () => {{ t == null || t.classList.remove(\"hovered\"); }}"\
        "}})()"), fmt::make_format_args(gear_idle.c_str(), gear_hover.c_str(), YAAGL_SETTINGS)));
}

void WAGL::OnDocumentLoaded(wxWebViewEvent& evt) {
    inject_stylesheet();
    inject_settings();
    Connect(web->GetId(), wxEVT_WEBVIEW_NAVIGATING,
            wxWebViewEventHandler(WAGL::OnNavigationRequest), NULL, this);
    Connect(web->GetId(), wxEVT_WEBVIEW_TITLE_CHANGED,
            wxWebViewEventHandler(WAGL::Width_Height_Workaround), NULL, this);
    Do_Background_And_Size(); // 3.1.5 makes away with this bullshit
}

void WAGL::Width_Height_Workaround(wxWebViewEvent& evt) {
    std::string parsing = evt.GetString().ToStdString();
    std::string width = parsing.substr(0, parsing.find(' '));
    parsing.replace(0, parsing.find(' ') + 1, "");
    std::string height = parsing;
    frame->SetSizeHints(wxSize(std::stoi(width), std::stoi(height)), wxSize(std::stoi(width), std::stoi(height)));
    frame->Show();
}

void WAGL::OnNavigationRequest(wxWebViewEvent& evt) {
    if(evt.GetURL().Contains(YAAGL_SETTINGS)) {
        evt.Veto();
        SettingsTest();
    }
}

bool WAGL::OnInit()
{
    if(!wxApp::OnInit())
        return false;
    frame = (new frmBaseline(nullptr));
    web = (wxWebView::New(frame.get(), wxID_ANY));
    frame->box->Add(web.get(), wxSizerFlags().Expand().Proportion(1));
    web->EnableHistory(false);
    web->EnableContextMenu(false);
#if (wxMAJOR_VERSION == 3) && (wxMINOR_VERSION >= 1) && (wxRELEASE_NUMBER >= 4)
    web->EnableAccessToDevTools(); // ayyyyyyyyyy
#endif
    Connect(web->GetId(), wxEVT_WEBVIEW_LOADED,
            wxWebViewEventHandler(WAGL::OnDocumentLoaded), NULL, this);
    web->LoadURL(generate_url());

    return true;
}